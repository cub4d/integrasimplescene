#include <osg/TexGen>
#include <osg/TexMat>
#include <osg/ShapeDrawable>
#include <osg/Geode>
#include <osg/MatrixTransform>
#include <osg/PositionAttitudeTransform>
#include <osg/LineSegment>
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osgUtil/IntersectVisitor>
#include <osgUtil/CullVisitor>
#include <osgUtil/Optimizer>

#include "SkyBox"

#include <osg/io_utils>
#include <sstream>
#include <algorithm>
#include <iterator>

osg::ref_ptr<osg::PositionAttitudeTransform> robot;
osg::ref_ptr<osg::PositionAttitudeTransform> house;
osg::ref_ptr<osg::Node> terrain;
osg::ref_ptr<osg::Group> scene;

const std::string TERRAIN_NAME = "p1_437";
const std::string   HOUSE_NAME = "polySurface73";

class PickHandler : public osgGA::GUIEventHandler
{
public:
    bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);
    virtual void pick(osgViewer::View* view, const osgGA::GUIEventAdapter& ea);
};

bool
PickHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
    if (ea.getEventType()  == osgGA::GUIEventAdapter::PUSH &&
        ea.getButtonMask() == osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON) 
    {
        osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
        if (view)
            pick(view, ea);
    }
    return false;
}

inline osg::Vec3
getRobotPos(const osg::Vec3 & pos)
{
    return osg::Vec3(pos.x(), pos.y(), pos.z() + robot->getBound().radius() / 2.0 - 0.5);
}

osg::AnimationPath*
createRobotAnimationPath(const osg::Vec3& goal)
{
    osg::ref_ptr<osg::AnimationPath> path = new osg::AnimationPath;
    path->setLoopMode(osg::AnimationPath::NO_LOOPING);

    osg::Vec3 pos = robot->getPosition();

    osg::Vec3 routeVec = goal - pos;
    routeVec.z() = 0;

    osg::Vec3 routeUnitVec(routeVec);
    routeUnitVec.normalize();

    double time = 0.0;
    const double DTIME = 0.05;

    osg::Quat rot; 
    rot.makeRotate(
            osg::Vec3(0, -1, 0), 
            routeUnitVec);

    while (routeVec.length() > 1.0) {
        path->insert(time, osg::AnimationPath::ControlPoint(pos, rot));
        
        time += DTIME;
        pos += routeUnitVec;
        
        osgUtil::LineSegmentIntersector* intersector =
            new osgUtil::LineSegmentIntersector(osg::Vec3(pos.x(), pos.y(),  999),
                                                osg::Vec3(pos.x(), pos.y(), -999));

        osgUtil::IntersectionVisitor iv(intersector);
        scene->accept(iv);

        auto intersections = intersector->getIntersections();
        for (auto&& i : intersections) {
            if (!i.nodePath.empty() && !i.nodePath.back()->getName().empty()) {
                std::string name = i.nodePath.back()->getName();
                if (name == TERRAIN_NAME) {
                    pos = getRobotPos( i.getWorldIntersectPoint() );

                    routeVec.x() = goal.x() - pos.x();
                    routeVec.y() = goal.y() - pos.y();
                }
                else if (name == HOUSE_NAME) {
                    return path.release();
                }
            }
        }
    }

    path->insert(time, osg::AnimationPath::ControlPoint( getRobotPos(goal), rot ));
    return path.release();
}

void
PickHandler::pick(osgViewer::View* view, const osgGA::GUIEventAdapter& ea)
{
    osgUtil::LineSegmentIntersector::Intersections intersections;

    if (view->computeIntersections(ea, intersections)) {
        for (auto&& i : intersections) {
            if (!i.nodePath.empty() && i.nodePath.back()->getName() == TERRAIN_NAME) {
                osg::AnimationPath* path = createRobotAnimationPath( i.getWorldIntersectPoint() );
                robot->setUpdateCallback(new osg::AnimationPathCallback(path));
                break;
            }
        }
    }
}

osg::Vec3
findTerrainHeight(double x, double y)
{
    osg::LineSegment* segment = new osg::LineSegment();
    segment->set(osg::Vec3(x, y,  999),
                 osg::Vec3(x, y, -999));

    osgUtil::IntersectVisitor iv;
    iv.addLineSegment(segment);
    terrain->accept(iv);

    osgUtil::IntersectVisitor::HitList hitlist;
    hitlist = iv.getHitList(segment);

    return osg::Vec3(hitlist.front().getWorldIntersectPoint());
}

struct TexMatCallback : public osg::NodeCallback
{
public:
    TexMatCallback(osg::TexMat& tm) :
            m_texMat(tm)
    {}

    virtual void operator()(osg::Node* node, osg::NodeVisitor* nv)
    {
        osgUtil::CullVisitor* cv = dynamic_cast<osgUtil::CullVisitor*>(nv);
        if (cv)
        {
            const osg::Matrix& MV = *cv->getModelViewMatrix();
            osg::Quat q = MV.getRotate();
            const osg::Matrix R = 
                osg::Matrix::rotate( q ) *
                osg::Matrix::rotate( osg::DegreesToRadians(90.0f), 1.0f, 0.0f, 0.0f );

            const osg::Matrix C = osg::Matrix::rotate( q.inverse() );

            m_texMat.setMatrix(C * R);

        }
        traverse(node, nv);
    }

    osg::TexMat& m_texMat;
};

int 
main(int argc, char** argv)
{
    scene = new osg::Group;

    terrain = osgDB::readNodeFile("data/terrain/terrain.flt");
    

    osg::ref_ptr<osg::Geode> geode = new osg::Geode;
    geode->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(), terrain->getBound().radius())));
    geode->setCullingActive(false);
    
    osg::ref_ptr<SkyBox> transform = new SkyBox;
    osg::TexMat* tm = new osg::TexMat;
    transform->getOrCreateStateSet()->setTextureAttributeAndModes(0, new osg::TexGen);
    transform->getOrCreateStateSet()->setTextureAttribute(0, tm);
    transform->setEnvironmentMap(0,
        osgDB::readImageFile("data/skybox/posx.tga"), osgDB::readImageFile("data/skybox/negx.tga"),
        osgDB::readImageFile("data/skybox/posy.tga"), osgDB::readImageFile("data/skybox/negy.tga"),
        osgDB::readImageFile("data/skybox/posz.tga"), osgDB::readImageFile("data/skybox/negz.tga"));
    transform->addChild(geode.get());

    osg::ref_ptr<osg::ClearNode> skybox = new osg::ClearNode;
    skybox->setCullCallback(new TexMatCallback(*tm));
    skybox->addChild(transform);


    osg::Light* skyLight = new osg::Light;
    skyLight->setPosition(osg::Vec4(-100.0, -100.0, 100.0, 1.0));
    skyLight->setAmbient(osg::Vec4(0.0f, 1.0f, 1.0f, 1.0f));
    skyLight->setDiffuse(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
    
    osg::ref_ptr<osg::LightSource> skyLightSource = new osg::LightSource;
    skyLightSource->setLight(skyLight);
    skyLightSource->setLocalStateSetModes(osg::StateAttribute::ON);
    skyLightSource->setStateSetModes(*scene->getOrCreateStateSet(), osg::StateAttribute::ON);


    robot = new osg::PositionAttitudeTransform;
    robot->addChild(osgDB::readNodeFile("data/robot/robot.osg"));
    robot->setPosition( getRobotPos(findTerrainHeight(0.0, 0.0)) );

    house = new osg::PositionAttitudeTransform;
    house->addChild(osgDB::readNodeFile("data/house/house.osg"));
    house->setPosition( findTerrainHeight(50.0, 50.0) );
    
    scene->addChild(        terrain.get() );
    scene->addChild(          robot.get() );
    scene->addChild(          house.get() );
    scene->addChild(         skybox.get() );
    scene->addChild( skyLightSource.get() );

    osgViewer::Viewer viewer;
    viewer.addEventHandler(new PickHandler);
    viewer.setSceneData(scene.get());

    return viewer.run();
}
